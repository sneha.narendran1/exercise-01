public class Rectangle {

    private float length;
    private float breadth;

    public Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public float getArea() {
        return length * breadth;
    }

    public float getPerimeter() {

        int numberOfTimes = 2;
        return numberOfTimes * (length + breadth);
    }

}
