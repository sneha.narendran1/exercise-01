import org.junit.Assert;
import org.junit.Test;


public class RectangleTest {

    private Rectangle rectangle;

    @Test
    public void shouldReturnOneAsAreaWhenThereAreUnitMeasurements() {
        rectangle = new Rectangle(1, 1);
        float area = rectangle.getArea();
        Assert.assertEquals(1, area, 0);
    }

    @Test
    public void shouldReturnTwoAsAreaWhenLengthIsOneAndBreadthIsTwo() {
        rectangle = new Rectangle(1, 2);
        float area = rectangle.getArea();
        Assert.assertEquals(2, area, 0);
    }

    @Test
    public void shouldReturnAreaAsSixWhenLengthIsTwoAndBreadthIsThree() {
        rectangle = new Rectangle(2, 3);
        float area = rectangle.getArea();
        Assert.assertEquals(6, area, 0);
    }

    @Test
    public void shouldReturnThePerimeterAsOneWhenThereUnitMeasurements() {
        rectangle = new Rectangle(1, 1);
        float perimeter = rectangle.getPerimeter();
        Assert.assertEquals(4, perimeter, 0);
    }

    @Test
    public void shouldReturnThePerimeterAs10When() {
        rectangle = new Rectangle(1, 1);
        float perimeter = rectangle.getPerimeter();
        Assert.assertEquals(4, perimeter, 0);

    }
}