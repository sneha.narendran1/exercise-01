import org.junit.Assert;
import org.junit.Test;


public class SquareTest {
    @Test
    public void shouldReturnAreaOneWithUnitMeasurements() {
        Square square = new Square(1);
        float area = square.getArea();
        Assert.assertEquals(1, area, 0);
    }

    @Test
    public void shouldReturnAreaFourWithTwoAsLength() {
        Square square = new Square(2);
        float area = square.getArea();
        Assert.assertEquals(4, area, 0);

    }

    @Test
    public void sh() {
    }
}